import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
//import 文件名 from 文件的路径
import Home from '@/components/Home'
import User from '@/components/User'
import News from '@/components/News'
import Detail from '@/components/Detail'
import Title from '@/components/Title'
import Mine from '@/components/mine'
import DataList from '@/components/dataList'
import Weather from '@/components/weather'
import Yule from '@/components/yule'

Vue.use(Router)

export default new Router({
  //mode:'history',//去掉#
  routes: [ 
    {
      path: '/home',
      component: Home
    },
    {
      path:'/user/:name',
      name:'我是用户',
      component:User
    },
    {
      path:'/news',
      component:News,
      alias:'/aaa',//别名 除了通过path地址也可以通过别名显示当前组件内容
      children:[
        {
          path: 'detail',
          component: Detail
        },
        {
          path: 'title',
          component: Title
        },
      ]
    },
    {
      path:'/mine/:name/:age/:sex',
      component:Mine
    },
    {
      path:'/dataList',
      component:DataList,
      children:[
        {
          path:'weather',
          component:Weather
        },
        {
          path:'yule',
          component:Yule
        }
      ]
    },
    {
      path:'*',
      redirect:'home' //重定向 除上述地址之外所有地址都被重新更改地址
    }
  ]
})
